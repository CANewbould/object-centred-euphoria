--------------------------------------------------------------------------------
-- dot.ex
--------------------------------------------------------------------------------
-- Notes:
--
--
--------------------------------------------------------------------------------
--/*
--= Application: dot.ex
-- Description: a basic pre-processor for Object-Centred Ephoria.
------
--[[[Version: 4.1.0.0
--Euphoria Versions: 4.0.5 upwards
--Author: C A Newbould
--Date: 2021.08.16
--Status: incomplete; operational
--Changes:]]]
--* created
--
--==Object-Centred Euphoria application: dot
--
--Run this application by keying:
-- <eucode>eui dot.ex <nameofsourcefile></eucode>
------
--*/
--------------------------------------------------------------------------------
--/*
--==Interface
--*/
--------------------------------------------------------------------------------
--/*
--=== Includes
--*/
--------------------------------------------------------------------------------
include ../include/object.e -- to access full range of OCE functionality
--------------------------------------------------------------------------------
--
--=== Constants
--
--------------------------------------------------------------------------------
constant CB = ')'
constant COMMA = ", "
constant DOT = '.'
constant OB = '('
constant SPACE = ' '
--------------------------------------------------------------------------------
--
--=== OCE types
--
--------------------------------------------------------------------------------
--
--=== Variables
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
--
--=== Routines
--
--------------------------------------------------------------------------------
function transpose(string line)
    -- need exclusions
    if matchIn(line, "include") then return line end if
    if startsWith(line, "--") then return line end if
    integer dot = findIn(line, DOT)
    --if findIn(line, '"') < dot then return line end if
    if dot then
--show(dot, "dot found at %d\n")
        integer openbracket = findIn(line, OB, dot)
        integer closebracket = findIn(line, CB, dot)
--show(openbracket, "open-bracket found at %d\n")
        string fn = line[dot+1..openbracket-1]
--show(fn, "fn name is: %s\n")
        integer previousspace = findIn(reverse(line[1..dot]), SPACE)
        if previousspace then previousspace = dot - previousspace + 1
        else previousspace = 0
        end if
--show(previousspace, "previous space found at %d\n")
        string obj = line[previousspace+1..dot-1]
/*
show(obj, "obj = %s\n")
show(line[1..previousspace] & fn & OB & obj, "first part = %s\n")
show(openbracket, "( is at %d;")
show(closebracket, ") is at %d\n")
show(iif(closebracket > openbracket+1, COMMA, ""), "optional bit = %s\n")
show(line[openbracket+1..$], "tail = %s") getc(0)
*/
        return line[1..previousspace] & fn & OB & obj
                    & iif(closebracket > openbracket+1, COMMA, "")
                    & line[openbracket+1..$]
    else return line -- no dot
    end if
end function
--------------------------------------------------------------------------------
procedure unopened(filename this)
    write("The file: " & this & " cannot be opened"
                & "Aborting when you key ENTER", WAIT)
    abort(0)
end procedure
--------------------------------------------------------------------------------
procedure main()
    sequence commline = command_line()
    -- insert test for length and prompt if short
    filehandle ih = open(commline[3])
    if ih then --OK
    else unopened(commline[3])
    end if
    string output = EMPTY_SEQUENCE
    while TRUE do
        object line = gets(ih)
        if atom(line) then exit -- END_OF_STREAM
        else line = transpose(line) --do transformation
        end if
        output &= line
    end while
    close(ih)
    filehandle oh = open("dottemp.ex", , TOWRITE)
    if ih then puts(oh, output)--OK
    else unopened("dottemp.ex")
    end if
    close(oh)
    write("!!! Using OCE's DOT notation pre-processor !!!\n\n")
    system("eui dottemp.ex", 0)
end procedure
--------------------------------------------------------------------------------
--
--=== Execution
--
--------------------------------------------------------------------------------
main()
--------------------------------------------------------------------------------
-- Previous versions
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
