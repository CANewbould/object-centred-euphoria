# Object-Centred Euphoria

Object-centred programming (OCP) - a new paradigm for computer programming.

What is Object-Centred Programming?
Are you a person, like me, who wishes there was a programming paradigm between OOP and procedural? If so, read on!

Object-Centred Programming is a concept which, like OOP, focusses on "object"s, and manipulates these using routines (designed to work for specific object types), but which leaves a programmer free to organise his/her program, or libraries, in ways which suit themselves.

## What is on offer here?
This site will eventually contain

* a wiki which explains the concept in outline
* a document outlining the concept
* a fully-defined (interpreted) language specification for an implementation of OCP
* a core language set
* a set of auxiliary modules (libraries adding extra functionality, if required)
* details of how to invoke the interpreter and how to create executables for distribution
* a set of test files with details of how to run them

A general outline follows.

## This implementation

This project utilises the Euphoria language (in its Open Euphoria clothing) to deliver a practical implementation of OCP: Object-Centred Euphoria (OCE).

In essence it is built on just two ideas:

* all objects are type-checked
* all routines are type-specific

These ideas are delivered as described below.

### Strategy

Open Euphoria offers the **type** concept, which, like classes in OOP, are is the key building block of OCP. Type-checking of variable values occurs by default.

Euphoria offers both **function**s and **procedure**s (void functions) but the emphasis in OCE will be on functions. A key element of the implementation is the use of *overloaded* routines; all routines exploit Open Euphoria's default-value argument system to maximise overloading and minimise the OE practice of cognate routine groups.

Fixed values in OCE are delivered through Open Euphoria's **constant** concept. Such values cannot be typed on declaration. So to enable fixed values to be type-checked, OCE encourages the use of **Constructors**, which, of course, can be used also to initialise, or re-assign, variable values. All constructors involve a type-checking step.

A specific use of fixed values is in defining special instances (Defined Instances). For example, St_Out represents the standard output stream.

Euphoria's only fundamental data-related distinction is between single (**atom**) values and multiple ones (**sequence**). All other defined types derive from these. So the structure of the OCE modules which provide the add-on functionality, above and beyond the Euphoria Core, follows this distinction, with separate modules: *atom.e* and *sequence.e*. A third module (*object.e*) sits "on top of" these two, passing on all the included functionality and adding a few routines which span this distinction. Thus, for all terminal-based coding the call to include *object.e* is the only one necessary.

A user of OCE has a choice with regard to adding their own (additional) modules:

* as the OCE library modules are read/write, the user can add their own code to any of the supplied ones as desired
* any new modules can sit on top of *object.e* simply by adding the call:

**public include object.e**

to his/her module.

In this regard, a library to provide a Graphical User Interface is also included in the project; to use it merely involves (public)ly including it in any calling code, as this automatically incorporates *object.e* and thus everything else.

### Naming conventions

Another (minor?) feature of OCE is using naming conventions as an additional element in code clarity.

The following conventions are used in OCE:

* CONSTANT[_IS_UPPER]
* typename[isrunon]
* Constructor[IsPascalCase]
* routines[AreCamelCase]
* variables[_are_snake_case]
* Defined_Instances[_Are_Darwin_Case]
* Namespaceistitlecase

### Documentation
OCE's modules are, to a degree, self-documenting. A separate application (written in OCE and included in the repository) converts the embedded code into html. The html files are included in the *docs* folder. Equally a user can study the source code in the module, reading the associated documentation directly.

### Dot notation

An example of a simple pre-processor (**dot.ex** in the *apps* folder) has been added in November 2021, along with a few examples of usage. The user will need to add a **eu.cfg** file in order to run these examples. Its syntax is:

`eui dot <filename.dx[w]>`

where filename stores the dotted code. This is simply a transformation of an OCE code:

`<routinename>(<objectname>,[<otherarguments>])`

into a dot-OCE one:

`<objectname>.<routinename>([<otherarguments>])`

Note that, as currently written, the pre-processor creates a local file, named **dottemp.ex**, which is run conventionally by Open Euphoria, within the pre-processor run. The file is left behind so that the user can see how the conversion process works in action. At present the pre-processor can only handle one dot per code line. The **dottemp.ex** is repeatedly overwritten with each call to **dot.ex**.

The plan is to develop a cleverer pre-processor in the future.

## The development process

The project will be developed dynamically, so frequent updates will take place. To follow progress keep watching this project!

## Contributions
If you would like to join the team then see below for how to do so.

## Who do I talk to?
Bitbucket user: CANewbould