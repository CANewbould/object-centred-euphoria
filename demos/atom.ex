/*
=Test bed for the OCE atom library

Tested successfully on:
* MS Windows
** OE4.1.0(64)
** OE4.1.0(32)
** OE4.0.5
* Linux
** OE4.1.0(64)

* Version 4.1.0.64.5
* Author: C A Newbould
* Date: 2020.04.29
* Status: complete; operational
* Changes:]]]
** modified to hold screen open when using //euiw//

==USES
*/

include object.e -- always include this sub-library

/*

==ROUTINES
*/

procedure test(atom this, sequence format={})   --v4.1.0.64.2
    show(this, format)
end procedure
-------------
procedure main(sequence heading) --v4.1.0.64.2
    show(repeat('-', lengthOf(heading)), "%s\n") --Using Core routine v4.1.0.64.2
    show(heading, "%s\n") --v4.1.0.64.2
    show(repeat('-', lengthOf(heading)), "%s\n") --Using Core routine v4.1.0.64.2
    test(4.71)   --v4.1.0.64.2
    test(13/7)   --v4.1.0.64.2
    test(13/7, "The division 13/7 yields %3.2f\n")   --v4.1.0.64.2
    show(abs(-8.2), "The absolute value of -8.2 is %g\n")   --v4.1.0.64.3
    write("\n*** Enter any key to close app *** ", WAIT) --v4.1.0.64.5
end procedure   --v4.1.0.64.4

/*

==RUN
*/
main("Testing atom routines") --v4.1.0.64.2
/*

==PREVIOUS VERSIONS
* Version 4.1.0.64.4
* Author: C A Newbould
* Date: 2020.01.24
* Status: complete; operational
* Changes:]]]
** modified include for Bitbucket repository

* Version 4.1.0.64.3
* Author: C A Newbould
* Date: 2019.12.30
* Status: incomplete; operational
* Changes:]]]
** test for ##abs## added

* Version 4.1.0.64.2
* Author: C A Newbould
* Date: 2019.11.29
* Status: incomplete; operational
* Changes:]]]
** ##main## renamed
** ##main## utilised in a different way

* Version 4.1.0.64.1
* Author: C A Newbould
* Date: 2019.11.22
* Status: incomplete; operational
* Changes:]]]
** modified to replace ##formatToScreen## with ##show##
** turned into a main routine

* Version 4.1.0.64.0
* Author: C A Newbould
* Date: 2019.11.13
* Status: incomplete; operational
* Changes:]]]
** created
*/
